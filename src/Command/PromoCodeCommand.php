<?php

namespace App\Command;


use App\Helper\PromoCodeFileWriterInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PromoCodeCommand extends Command
{
	private const CODE = 'code';

	protected static $defaultName = 'promo-code:validate';

	private PromoCodeFileWriterInterface $fileWriter;

	public function __construct(PromoCodeFileWriterInterface $writer)
	{
		$this->fileWriter = $writer;

		parent::__construct();
	}

	protected function configure(): void
	{
		$this
			->setDescription('Validates a promo code.')
			->setHelp('This command allows you to validate a promo code...')
			->addArgument(self::CODE, InputArgument::REQUIRED, 'The promo code you want to validate.')
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		$output->writeln([
			'EKWATEUR PROMO CODE VALIDATOR',
			'==============================',
			'',
		]);

		$promoCode = $input->getArgument(self::CODE);

		try {
			$promoCodeFile = $this->fileWriter->writeDetailsToFile($promoCode);
			$output->writeln("Promo code details written to file: " . $promoCodeFile);
		} catch (\Throwable $e) {
			$output->writeln("Error: " . $e->getMessage());
			return Command::FAILURE;
		}

		$output->writeln("TERMINATED WITH SUCCESS.");
		return Command::SUCCESS;
	}
}
