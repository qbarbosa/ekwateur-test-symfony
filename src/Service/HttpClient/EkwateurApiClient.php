<?php

namespace App\Service\HttpClient;


use App\Helper\EkwateurApiInterface;
use App\Service\Exceptions\HttpClient\HttpTransportException;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class EkwateurApiClient implements EkwateurApiInterface
{
	private HttpClientInterface $client;
	private string $baseUrl = 'https://601025826c21e10017050013.mockapi.io/ekwatest/';

	public function __construct(HttpClientInterface $client)
	{
		$this->client = $client;
	}

	private function getData(string $endpoint): array
	{
		try {
			$response = $this->client->request(
				'GET',
				$this->baseUrl . $endpoint
			);

			return $response->toArray();
		} catch (\Throwable $e) {
			throw new HttpTransportException($e->getMessage());
		}
	}

	public function getPromoCodeList(): array
	{
		return $this->getData('promoCodeList');
	}

	public function getOfferList(): array
	{
		return $this->getData('offerList');
	}
}