<?php

namespace App\Service;


use App\Helper\OfferRetrieverInterface;
use App\Helper\PromoCodeFileWriterInterface;
use App\Helper\PromoCodeRetrieverInterface;
use Symfony\Component\Filesystem\Filesystem;

class PromoCodeFileWriter implements PromoCodeFileWriterInterface
{
	private PromoCodeRetrieverInterface $promoCodeService;
	private OfferRetrieverInterface $offerService;

	public function __construct(PromoCodeRetrieverInterface $promoCodeService, OfferRetrieverInterface $offerService)
	{
		$this->promoCodeService = $promoCodeService;
		$this->offerService = $offerService;
	}

	public function writeDetailsToFile(string $promoCode): string
	{
		$promoCodeDetails = $this->getPromoCodeDetails($promoCode);

		$fileName = 'var/' . $promoCodeDetails['promoCode'] . '.json';
		$this->writeToFile($fileName, $promoCodeDetails);

		return $fileName;
	}

	private function getPromoCodeDetails(string $promoCode): array
	{
		$code = $this->promoCodeService->retrievePromoCode($promoCode);
		$offers = $this->offerService->retrieveCompatibleOffers($promoCode);

		$code['compatibleOfferList'] = $offers['compatibleOfferList'];

		return $code;
	}

	private function writeToFile(string $fileName, array $promoCodeDetails): void
	{
		$promoCodeJson = json_encode($promoCodeDetails);

		$fileSystem = new Filesystem();
		$fileSystem->dumpFile($fileName, $promoCodeJson);
	}
}