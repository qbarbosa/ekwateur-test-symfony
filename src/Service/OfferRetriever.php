<?php

namespace App\Service;


use App\Helper\EkwateurApiInterface;
use App\Helper\OfferRetrieverInterface;
use App\Service\Exceptions\OfferRetriever\EmptyOfferListException;
use App\Service\Exceptions\OfferRetriever\NoCompatibleOfferException;

class OfferRetriever implements OfferRetrieverInterface
{
	private EkwateurApiInterface $client;

	public function __construct(EkwateurApiInterface $client)
	{
		$this->client = $client;
	}

	public function retrieveCompatibleOffers(string $promoCode): array
	{
		$offerList = $this->retrieveOfferList();

		return $this->getCompatibleOffers($promoCode, $offerList);
	}

	private function retrieveOfferList(): array
	{
		$list = $this->client->getOfferList();

		if (empty($list))
			throw new EmptyOfferListException();

		return $list;
	}

	private function getCompatibleOffers(string $code, array $offerList): array
	{
		$compatibleOfferList = [];

		// Get offers associated to promo code
		foreach ($offerList as $offer) {
			if (
				array_key_exists('validPromoCodeList', $offer)
				&& in_array($code, $offer['validPromoCodeList'])
			) {
				$compatibleOfferList[] = [
					'name' => $offer['offerName'],
					'type' => $offer['offerType']
				];
			}
		}

		if (empty($compatibleOfferList))
			throw new NoCompatibleOfferException();

		return [
			'promoCode' => $code,
			'compatibleOfferList' => $compatibleOfferList
		];
	}
}