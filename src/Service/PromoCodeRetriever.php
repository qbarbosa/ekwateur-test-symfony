<?php

namespace App\Service;


use App\Helper\EkwateurApiInterface;
use App\Helper\PromoCodeRetrieverInterface;
use App\Service\Exceptions\PromoCodeRetriever\EmptyPromoCodeListException;
use App\Service\Exceptions\PromoCodeRetriever\NotExistingPromoCodeException;
use App\Service\Exceptions\PromoCodeRetriever\OutdatedPromoCodeException;

class PromoCodeRetriever implements PromoCodeRetrieverInterface
{
	private EkwateurApiInterface $client;

	public function __construct(EkwateurApiInterface $client)
	{
		$this->client = $client;
	}

	public function retrievePromoCode(string $promoCode): array
	{
		$promoCodeList = $this->retrievePromoCodeList();

		$promoCodeArray = $this->getPromoCodeFromList($promoCode, $promoCodeList);

		if (!$this->promoCodeIsValid($promoCodeArray)) {
			throw new OutdatedPromoCodeException();
		}

		return [
			'promoCode'     => $promoCodeArray['code'],
			'endDate'       => $promoCodeArray['endDate'],
			'discountValue' => $promoCodeArray['discountValue'],
		];
	}

	private function retrievePromoCodeList(): array
	{
		$list = $this->client->getPromoCodeList();

		if (empty($list))
			throw new EmptyPromoCodeListException();

		return $list;
	}

	private function getPromoCodeFromList(string $promoCode, array $promoCodeList): array
	{
		foreach ($promoCodeList as $p) {
			if ($p['code'] === $promoCode)
				return $p;
		}

		throw new NotExistingPromoCodeException();
	}

	private function promoCodeIsValid(array $promoCodeArray): bool
	{
		$today  = strtotime(date("Y-m-d"));
		$expire = strtotime($promoCodeArray['endDate']);

		return ($today <= $expire);
	}
}