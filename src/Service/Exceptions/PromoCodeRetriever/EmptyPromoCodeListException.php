<?php

namespace App\Service\Exceptions\PromoCodeRetriever;


use RuntimeException;

class EmptyPromoCodeListException extends RuntimeException
{
	protected $code = 1000;
	protected $message = "Fetched promo code list is empty.";
}
