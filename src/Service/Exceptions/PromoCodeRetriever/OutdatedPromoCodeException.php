<?php

namespace App\Service\Exceptions\PromoCodeRetriever;


use RuntimeException;

class OutdatedPromoCodeException extends RuntimeException
{
	protected $code = 1002;
	protected $message = "Promo code is outdated.";
}
