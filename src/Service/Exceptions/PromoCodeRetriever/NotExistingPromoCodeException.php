<?php

namespace App\Service\Exceptions\PromoCodeRetriever;


use RuntimeException;

class NotExistingPromoCodeException extends RuntimeException
{
	protected $code = 1001;
	protected $message = "Promo code does not figure in list.";
}
