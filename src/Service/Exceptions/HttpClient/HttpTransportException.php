<?php

namespace App\Service\Exceptions\HttpClient;


use RuntimeException;
use Throwable;

class HttpTransportException extends RuntimeException
{
	protected $code = 9000;
	protected $message = "Something bad happened while retrieving content.";

	public function __construct($additionalMessage = '', Throwable $previous = null)
	{
		if ($additionalMessage) {
			$this->message .= sprintf(': %s', $additionalMessage);
		}
		parent::__construct($this->message, $this->code, $previous);
	}

}
