<?php

namespace App\Service\Exceptions\OfferRetriever;


use RuntimeException;

class NoCompatibleOfferException extends RuntimeException
{
	protected $code = 2001;
	protected $message = "Promo code has no compatible offer.";
}