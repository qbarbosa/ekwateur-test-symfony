<?php

namespace App\Service\Exceptions\OfferRetriever;


use RuntimeException;

class EmptyOfferListException extends RuntimeException
{
	protected $code = 2000;
	protected $message = "Fetched offer list is empty.";
}