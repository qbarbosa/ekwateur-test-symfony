<?php

namespace App\Helper;


Interface EkwateurApiInterface
{
	public function getPromoCodeList(): array;
	public function getOfferList(): array;
}
