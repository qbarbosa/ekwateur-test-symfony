<?php

namespace App\Helper;


Interface PromoCodeRetrieverInterface
{
	public function retrievePromoCode(string $promoCode): array;
}
