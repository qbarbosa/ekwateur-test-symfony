<?php

namespace App\Helper;


Interface PromoCodeFileWriterInterface
{
	public function writeDetailsToFile(string $promoCode): string;
}
