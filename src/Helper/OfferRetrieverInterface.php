<?php

namespace App\Helper;


Interface OfferRetrieverInterface
{
	public function retrieveCompatibleOffers(string $promoCode): array;
}
