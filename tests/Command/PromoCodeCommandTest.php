<?php

namespace App\Tests\Command;


use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class PromoCodeCommandTest extends KernelTestCase
{
	public function testExecute()
	{
		$kernel = static::createKernel();
		$application = new Application($kernel);

		$command = $application->find('promo-code:validate');
		$commandTester = new CommandTester($command);
		$commandTester->execute([
			'code' => 'THIS_IS_A_TEST_PROMO_CODE',
		]);

		$output = $commandTester->getDisplay();
		$this->assertStringContainsString('EKWATEUR PROMO CODE VALIDATOR', $output);
	}

	public function testFail()
	{
		$this->expectException(\Throwable::class);

		$kernel = static::createKernel();
		$application = new Application($kernel);
		$command = $application->find('promo-code:validate');

		$commandTester = new CommandTester($command);
		$commandTester->execute([]);
	}
}