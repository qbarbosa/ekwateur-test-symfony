<?php

namespace App\Tests\Service;


use App\Service\HttpClient\EkwateurApiClient;
use App\Service\Exceptions\PromoCodeRetriever\NotExistingPromoCodeException;
use App\Service\Exceptions\PromoCodeRetriever\OutdatedPromoCodeException;
use App\Service\PromoCodeRetriever;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class PromoCodeRetrieverTest extends KernelTestCase
{
	public function testPromoCodeIsValidSuccess()
	{
		$validator = new PromoCodeRetriever($this->getMockedApiClient());

		$promoCode = 'TEST_PROMO_CODE_SUCCESS';
		$result = $validator->retrievePromoCode($promoCode);

		$this->assertIsArray($result);
		$this->assertNotEmpty($result);
		$this->assertArrayHasKey('promoCode', $result);
		$this->assertArrayHasKey('discountValue', $result);
		$this->assertArrayHasKey('endDate', $result);
	}

	public function testPromoCodeIsValidOutdated()
	{
		$this->expectException(OutdatedPromoCodeException::class);

		$validator = new PromoCodeRetriever($this->getMockedApiClient());

		$promoCode = 'TEST_PROMO_CODE_OUTDATED';
		$validator->retrievePromoCode($promoCode);
	}

	public function testPromoCodeIsValidNotExisting()
	{
		$this->expectException(NotExistingPromoCodeException::class);

		$validator = new PromoCodeRetriever($this->getMockedApiClient());

		$promoCode = 'TEST_PROMO_CODE_DOES_NOT_EXIST';
		$validator->retrievePromoCode($promoCode);
	}

	private function getMockedApiClient(): EkwateurApiClient
	{
		$client = $this->createMock(EkwateurApiClient::class);

		$client
			->expects($this->once())
			->method('getPromoCodeList')
			->will($this->returnValue([
				[
					"code" => "TEST_PROMO_CODE_SUCCESS",
					"discountValue" => 2,
					"endDate" => "2030-01-01"
				],
				[
					"code" => "TEST_PROMO_CODE_OUTDATED",
					"discountValue" => 1.5,
					"endDate" => "2015-01-01"
				]
			]));

		return $client;
	}
}