<?php

namespace App\Tests\Service\HttpClient;


use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Service\HttpClient\EkwateurApiClient;

class EkwateurApiClientTest extends KernelTestCase
{
	public function testGetPromoCodeList()
	{
		/** @var EkwateurApiClient $client */
		$client = static::getContainer()->get(EkwateurApiClient::class);
		$codeList = $client->getPromoCodeList();

		$this->assertIsArray($codeList);
		$this->assertNotEmpty($codeList);
		$this->assertArrayHasKey('code', $codeList[0]);
		$this->assertArrayHasKey('endDate', $codeList[0]);
	}

	public function testGetOfferList()
	{
		/** @var EkwateurApiClient $client */
		$client = static::getContainer()->get(EkwateurApiClient::class);
		$offerList = $client->getOfferList();

		$this->assertIsArray($offerList);
		$this->assertNotEmpty($offerList);
		$this->assertArrayHasKey('offerType', $offerList[0]);
		$this->assertArrayHasKey('offerName', $offerList[0]);
		$this->assertArrayHasKey('offerDescription', $offerList[0]);
		$this->assertArrayHasKey('validPromoCodeList', $offerList[0]);
	}
}