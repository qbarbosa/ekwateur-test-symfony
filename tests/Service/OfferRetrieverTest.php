<?php

namespace App\Tests\Service;


use App\Helper\EkwateurApiInterface;
use App\Service\Exceptions\OfferRetriever\EmptyOfferListException;
use App\Service\Exceptions\OfferRetriever\NoCompatibleOfferException;
use App\Service\HttpClient\EkwateurApiClient;
use App\Service\OfferRetriever;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class OfferRetrieverTest extends KernelTestCase
{
	public function testRetrieveOffersSuccess()
	{
		$offerRetriever = new OfferRetriever(
			$this->getMockedApiClient()
		);

		$promoCode = 'TEST_PROMO_CODE_SUCCESS';
		$result = $offerRetriever->retrieveCompatibleOffers($promoCode);

		$this->assertIsArray($result);
		$this->assertNotEmpty($result);
		$this->assertArrayHasKey('promoCode', $result);
		$this->assertArrayHasKey('compatibleOfferList', $result);

		$this->assertEquals($result,
			[
				"promoCode" => "TEST_PROMO_CODE_SUCCESS",
				"compatibleOfferList" => [
					[
						"name" => "EKWAG2000",
						"type" => "GAS"
					],
					[
						"name" => "EKWAE2000",
						"type" => "ELECTRICITY"
					]
				]
			]
		);
	}

	public function testNoCompatibleOffer()
	{
		$this->expectException(NoCompatibleOfferException::class);

		$offerRetriever = new OfferRetriever(
			$this->getMockedApiClient()
		);

		$promoCode = 'TEST_PROMO_CODE_WITH_NO_MATCH';
		$offerRetriever->retrieveCompatibleOffers($promoCode);
	}

	public function testNoOfferFromApi()
	{
		$this->expectException(EmptyOfferListException::class);

		$offerRetriever = new OfferRetriever(
			$this->getMockedApiClientEmptyResult()
		);

		$promoCode = 'TEST_PROMO_CODE_NO_OFFER';
		$offerRetriever->retrieveCompatibleOffers($promoCode);
	}

	private function getMockedApiClient(): EkwateurApiInterface
	{
		$client = $this->createMock(EkwateurApiClient::class);

		$client
			->expects($this->once())
			->method('getOfferList')
			->will($this->returnValue([
				[
					"offerType" => "GAS",
					"offerName" => "EKWAG2000",
					"offerDescription" => "Une offre incroyable",
					"validPromoCodeList" => [
						"TEST_PROMO_CODE_SUCCESS",
						"ALL_2000"
					]
				],
				[
					"offerType" => "ELECTRICITY",
					"offerName" => "EKWAE2000",
					"offerDescription" => "Une offre du tonnerre",
					"validPromoCodeList" => [
						"TEST_PROMO_CODE_SUCCESS",
						"ALL_2000",
						"ELEC_IS_THE_NEW_GAS"
					]
				]
			]));

		return $client;
	}

	private function getMockedApiClientEmptyResult(): EkwateurApiInterface
	{
		$client = $this->createMock(EkwateurApiClient::class);

		$client
			->expects($this->once())
			->method('getOfferList')
			->will($this->returnValue([]));

		return $client;
	}
}