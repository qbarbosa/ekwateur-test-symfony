<?php

namespace App\Tests\Service;


use App\Helper\OfferRetrieverInterface;
use App\Helper\PromoCodeRetrieverInterface;
use App\Service\OfferRetriever;
use App\Service\PromoCodeFileWriter;
use App\Service\PromoCodeRetriever;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\File\File;

class PromoCodeFileWriterTest extends KernelTestCase
{
	public function testWriterSuccess()
	{
		$writer = new PromoCodeFileWriter(
			$this->getMockedPromoService(),
			$this->getMockedOfferService()
		);

		$promoCode = 'TEST_PROMO_CODE_SUCCESS';
		$writer->writeDetailsToFile($promoCode);

		$expectedFilename = 'var/TEST_PROMO_CODE_SUCCESS.json';
		$expectedContent = '{"promoCode":"TEST_PROMO_CODE_SUCCESS","endDate":"2030-01-01","discountValue":2.5,"compatibleOfferList":[{"name":"EKWAG2000","type":"GAS"},{"name":"EKWAE2000","type":"ELECTRICITY"}]}';

		$this->assertFileExists($expectedFilename);
		$file = new File($expectedFilename);
		$this->assertEquals(
			$file->getContent(),
			$expectedContent
		);
	}

	private function getMockedPromoService(): PromoCodeRetrieverInterface
	{
		$promoCodeService = $this->createMock(PromoCodeRetriever::class);

		$promoCodeService
			->expects($this->once())
			->method('retrievePromoCode')
			->with('TEST_PROMO_CODE_SUCCESS')
			->will($this->returnValue([
				"promoCode"     => "TEST_PROMO_CODE_SUCCESS",
				"endDate"       => "2030-01-01",
				"discountValue" => 2.5
			]));

		return $promoCodeService;
	}

	private function getMockedOfferService(): OfferRetrieverInterface
	{
		$offerService = $this->createMock(OfferRetriever::class);

		$offerService
			->expects($this->once())
			->method('retrieveCompatibleOffers')
			->with('TEST_PROMO_CODE_SUCCESS')
			->will($this->returnValue([
				"promoCode"           => "TEST_PROMO_CODE_SUCCESS",
				"compatibleOfferList" => [
					[
						"name" => "EKWAG2000",
						"type" => "GAS"
					],
					[
						"name" => "EKWAE2000",
						"type" => "ELECTRICITY"
					],
				]
			]));

		return $offerService;
	}

}