# Ekwateur Makefile
# Contains handful docker shortcuts.

IMG_NAME = ekwateur-php-image
APP_NAME = ekwateur-promo-app

# Prints the help summary for a given .mk file
define print_summary
	echo "\n\033[1m$(1)\033[0m"
	egrep '^[a-zA-Z_-]+:.*?## .*$$' $(2) | awk 'BEGIN {FS = ":.*?## "}; {printf " \033[36m%-20s\033[0m %s\n", $$1, $$2}'
endef

help: ## Shows this help message.
	@echo "\n  \033[4mEkwateur Test Makefile\033[0m\n\n\033[1mUsage:\033[0m\n\n  make \033[36mtarget\033[0m"
	@$(call print_summary,Parameters list,Makefile)

build: ## Builds docker image.
	docker build -t ${IMG_NAME} .

up: ## Runs container. Please run `make build` before.
	docker run -d --rm --name ${APP_NAME} -v ${PWD}:/usr/src/app ${IMG_NAME}

stop: ## Stops container.
	docker stop ${APP_NAME}

vendor: ## Installs composer dependencies.
	docker exec -it ${APP_NAME} composer install

sh: ## Opens interactive shell in container.
	docker exec -it ${APP_NAME} /bin/sh

test: vendor ## Runs unit tests.
	docker exec -it ${APP_NAME} bin/phpunit

promo-cmd: vendor ## Executes command `promo-code:validate` with given promo code. Ex: make promo-cmd code='EXAMPLE_PROMO_CODE'
	docker exec -it ${APP_NAME} bin/console promo-code:validate $(code)

.PHONY: help sh test exec
